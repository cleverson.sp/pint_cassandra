package br.com.Imersao.cassproj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CassprojApplication {

	public static void main(String[] args) {
		SpringApplication.run(CassprojApplication.class, args);
	}

}

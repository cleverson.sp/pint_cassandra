package br.com.Imersao.cassproj.controllers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import br.com.Imersao.cassproj.dtos.LoginResposta;
import br.com.Imersao.cassproj.models.Login;
import br.com.Imersao.cassproj.services.UsuarioService;

@RestController
@RequestMapping("/login")
public class LoginController {
	
	@Autowired
	UsuarioService usuarioService;
	
	@PostMapping
	public LoginResposta fazerLogin(@RequestBody Login login){
		Optional<String> tokenOptional = usuarioService.fazerLogin(login);
		
		if(tokenOptional.isPresent()) {
			LoginResposta loginResposta = new LoginResposta();
			loginResposta.setToken(tokenOptional.get());
			return loginResposta;
		}
		
		throw new ResponseStatusException(HttpStatus.UNAUTHORIZED);
		

		
		
	}
	
}

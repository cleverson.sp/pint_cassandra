package br.com.Imersao.cassproj.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.Imersao.cassproj.dtos.Pin;
import br.com.Imersao.cassproj.models.PinFavorito;
import br.com.Imersao.cassproj.models.PinUsuario;
import br.com.Imersao.cassproj.services.PinService;

@RestController
@RequestMapping("/pin")
public class PinController {

	@Autowired
	private PinService pinService;

	@PostMapping("/cadastro/{usuario}")
	public PinUsuario cadastrarPin(@PathVariable String usuario, @RequestBody Pin pin) {

		return pinService.cadastrarPin(pin, usuario);
	}

	@PostMapping("/favoritar")
	public PinFavorito favoritar(@RequestBody PinFavorito favorito) {

		return pinService.favoritar(favorito);

	}

}

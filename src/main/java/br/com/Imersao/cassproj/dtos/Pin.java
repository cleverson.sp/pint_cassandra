package br.com.Imersao.cassproj.dtos;

import java.util.UUID;

import br.com.Imersao.cassproj.enums.Categoria;

public class Pin {
	
	private String imagem;
	private String descricao;
	private Categoria categoria;
	private UUID dataPost;

	
	public String getImagem() {
		return imagem;
	}
	public void setImagem(String imagem) {
		this.imagem = imagem;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public Categoria getCategoria() {
		return categoria;
	}
	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	public UUID getDataPost() {
		return dataPost;
	}
	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}
	
}

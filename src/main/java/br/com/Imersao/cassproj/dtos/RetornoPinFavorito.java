package br.com.Imersao.cassproj.dtos;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;

import br.com.Imersao.cassproj.enums.Categoria;

public class RetornoPinFavorito {

	private String usuario;
	private String imagem;
	private String descricao;
	private Categoria categoria;
	private String donoPostPint;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getDonoPostPint() {
		return donoPostPint;
	}

	public void setDonoPostPint(String donoPostPint) {
		this.donoPostPint = donoPostPint;
	}

	public UUID getDataPost() {
		return dataPost;
	}

	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}

	public UUID getDataPostFav() {
		return dataPostFav;
	}

	public void setDataPostFav(UUID dataPostFav) {
		this.dataPostFav = dataPostFav;
	}

	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal = 2)
	private UUID dataPost;
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal = 1)
	private UUID dataPostFav;

}

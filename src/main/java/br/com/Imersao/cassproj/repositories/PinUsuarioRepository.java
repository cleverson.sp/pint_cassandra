package br.com.Imersao.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.cassproj.models.PinUsuario;


public interface PinUsuarioRepository extends CrudRepository<PinUsuario, String>{
	
	

}
package br.com.Imersao.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.cassproj.models.PinFavorito;

public interface PinFavoritoRepository extends CrudRepository<PinFavorito, String> {

}

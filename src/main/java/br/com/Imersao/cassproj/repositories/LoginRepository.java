package br.com.Imersao.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.cassproj.models.Login;

public interface LoginRepository extends CrudRepository<Login, String> {

}

package br.com.Imersao.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.cassproj.enums.Categoria;
import br.com.Imersao.cassproj.models.PinCategoria;

public interface PinCategoriaRepository extends CrudRepository<PinCategoria, Categoria>{

}

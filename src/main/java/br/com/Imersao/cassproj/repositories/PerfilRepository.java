package br.com.Imersao.cassproj.repositories;

import org.springframework.data.repository.CrudRepository;

import br.com.Imersao.cassproj.models.Perfil;

public interface PerfilRepository extends CrudRepository<Perfil, String>{

}

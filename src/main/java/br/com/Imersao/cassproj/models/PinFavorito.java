package br.com.Imersao.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import br.com.Imersao.cassproj.enums.Categoria;
@Table
public class PinFavorito {

	@PrimaryKeyColumn(type = PrimaryKeyType.PARTITIONED)
	private String usuario;

	private String imagem;
	private String descricao;
	private Categoria categoria;
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal =0)
	private UUID dataPost;
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal =1)
	private String postador;
	@PrimaryKeyColumn(type = PrimaryKeyType.CLUSTERED, ordinal =2)
	private UUID DataFav;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public UUID getDataPost() {
		return dataPost;
	}

	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}

	public String getPostador() {
		return postador;
	}

	public void setPostador(String postador) {
		this.postador = postador;
	}

	public UUID getDataFav() {
		return DataFav;
	}

	public void setDataFav(UUID dataFav) {
		DataFav = dataFav;
	}

}

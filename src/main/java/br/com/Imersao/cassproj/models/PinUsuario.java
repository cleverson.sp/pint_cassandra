package br.com.Imersao.cassproj.models;

import java.util.UUID;

import org.springframework.data.cassandra.core.cql.PrimaryKeyType;
import org.springframework.data.cassandra.core.mapping.PrimaryKeyColumn;
import org.springframework.data.cassandra.core.mapping.Table;

import br.com.Imersao.cassproj.enums.Categoria;

@Table
public class PinUsuario {
	
	@PrimaryKeyColumn(type=PrimaryKeyType.PARTITIONED)
	private String usuario;
	@PrimaryKeyColumn(type=PrimaryKeyType.CLUSTERED, ordinal=1)
	private UUID dataPost;
	
	private String imagem;
	private String descricao;
	private Categoria categoria;

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public UUID getDataPost() {
		return dataPost;
	}

	public void setDataPost(UUID dataPost) {
		this.dataPost = dataPost;
	}

	public String getImagem() {
		return imagem;
	}

	public void setImagem(String imagem) {
		this.imagem = imagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Categoria getCategoria() {
		return categoria;
	}

	public void setCategoria(Categoria categoria) {
		this.categoria = categoria;
	}
	
	

}

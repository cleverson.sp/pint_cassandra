package br.com.Imersao.cassproj.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import br.com.Imersao.cassproj.dtos.Usuario;
import br.com.Imersao.cassproj.models.Login;
import br.com.Imersao.cassproj.models.Perfil;
import br.com.Imersao.cassproj.models.PinUsuario;
import br.com.Imersao.cassproj.repositories.LoginRepository;
import br.com.Imersao.cassproj.repositories.PerfilRepository;
import br.com.Imersao.cassproj.security.JwtTokenProvider;

@Service
public class UsuarioService {

	@Autowired
	private LoginRepository loginRepository;

	@Autowired
	private JwtTokenProvider tokenProvider;

	@Autowired
	private PerfilRepository perfilRepository;

	private BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();

	public Optional<String> fazerLogin(Login login) {

		Optional<Login> loginOptional = loginRepository.findById(login.getUsuario());

		if (loginOptional.isPresent()) {
			if (encoder.matches(login.getSenha(), loginOptional.get().getSenha())) {
				String token = tokenProvider.criarToken(login.getUsuario());
				return Optional.of(token);
			}
		}

		return Optional.empty();
	}
	
	public void cadastrar(Usuario usuario) {

		cadastrarLogin(usuario);
		cadastrarPerfil(usuario);

	}

	private void cadastrarLogin(Usuario usuario) {

		Login login = new Login();
		System.out.println("----->");
		String hash = encoder.encode(usuario.getSenha());

		login.setUsuario(usuario.getUsuario());
		login.setSenha(hash);

		loginRepository.save(login);

	}

	private void cadastrarPerfil(Usuario usuario) {
		Perfil perfil = new Perfil();

		perfil.setUsuario(usuario.getUsuario());
		perfil.setNome(usuario.getNome());
		perfil.setFotoPerfil(usuario.getFotoPerfil());
		perfil.setBio(usuario.getBio());

		perfilRepository.save(perfil);

	}
	
	public Usuario consultarUsuario(String usuario) {
		
		
		return .findById(usuario);	
	}

}

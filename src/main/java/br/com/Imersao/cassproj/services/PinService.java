package br.com.Imersao.cassproj.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.datastax.driver.core.utils.UUIDs;

import br.com.Imersao.cassproj.dtos.Pin;
import br.com.Imersao.cassproj.models.PinCategoria;
import br.com.Imersao.cassproj.models.PinFavorito;
import br.com.Imersao.cassproj.models.PinUsuario;
import br.com.Imersao.cassproj.repositories.PinCategoriaRepository;
import br.com.Imersao.cassproj.repositories.PinFavoritoRepository;
import br.com.Imersao.cassproj.repositories.PinUsuarioRepository;

@Service
public class PinService {

	@Autowired
	PinUsuarioRepository pinUsuarioRepository;

	@Autowired
	PinCategoriaRepository pinCategoriaRepository;

	@Autowired
	PinFavoritoRepository pinFavoritoRepository;

	public PinUsuario cadastrarPin(Pin pin, String usuario) {

		PinUsuario pinUsuario = new PinUsuario();

		pinUsuario.setCategoria(pin.getCategoria());
		pinUsuario.setDescricao(pin.getDescricao());
		pinUsuario.setImagem(pin.getImagem());
		pinUsuario.setDataPost(UUIDs.timeBased());
		pinUsuario.setUsuario(usuario);
		pinUsuarioRepository.save(pinUsuario);

		PinCategoria pinCategoria = new PinCategoria();
		pinCategoria.setCategoria(pin.getCategoria());
		pinCategoria.setDescricao(pin.getDescricao());
		pinCategoria.setImagem(pin.getImagem());
		pinCategoria.setDataPost(UUIDs.timeBased());
		pinCategoria.setUsuario(usuario);
		pinCategoriaRepository.save(pinCategoria);

		return pinUsuario;

	}

	public PinFavorito favoritar(PinFavorito pinFavorito) {

		
		
		
		pinFavorito.setDataFav(UUIDs.timeBased());
		return pinFavoritoRepository.save(pinFavorito);

		 

	}

}
